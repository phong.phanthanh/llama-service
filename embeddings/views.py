from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
import sys
from langchain.embeddings import LlamaCppEmbeddings

llama = LlamaCppEmbeddings(model_path="./llama.bin")


@api_view(['POST'])
def embed_document(request):
    print('embedding document')

    doc_result = llama.embed_documents([request.data['document']])

    return Response(doc_result)


@api_view(['POST'])
def embed_documents(request):
    doc_result = llama.embed_documents([request.data['documents']])

    return Response(doc_result)


@api_view(['POST'])
def embed_query(request):
    doc_result = llama.embed_query([request.data['query']])

    return Response(doc_result)
