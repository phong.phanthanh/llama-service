from django.urls import path

from . import views

urlpatterns = [
    path("embed-document", views.embed_document, name="embed_document"),
    path("embed-documents", views.embed_documents, name="embed_documents"),
    path("embed-query", views.embed_query, name="embed_quey"),
]
